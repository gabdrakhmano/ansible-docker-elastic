# ansible-docker-elastic


## Задание

Elastic Stack:
- запустить Elasticsearch
- запустить Kibana и подключить к Elasticsearch
- установить filebeat
- настроить filebeat для чтения системных журналов с отправкой логов в Elasticsearch
- настроить filebeat для чтения своих же логов (filebeat) с отправкой логов в Elasticsearch в другой индекс

Дополнительные задания (по желанию)
- из предыдущего задания у нас должно быть два разных индекса. Необходимо настроить ротацию в 1 день для каждого из этих индексов
- сделать pattern в kibana для каждого индекса, чтобы в discovery можно было переключать поиск по индексам

В помощь к домашке.

https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-installation-configuration.html

https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html

https://www.elastic.co/guide/en/logstash/current/installing-logstash.html

https://www.elastic.co/guide/en/kibana/current/install.html

## Add your files
